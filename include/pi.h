#ifndef PI_H
#define PI_H

void *hilo(void* params);
double calcPiMono();
double calcPiMulti();

#endif
