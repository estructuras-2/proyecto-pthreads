#ifndef MATRIZVECTOR_H
#define MATRIZVECTOR_H

void inicializacion();
void *thread_routine(void *arg);
int vectorResultanteNormal();
int vectorResultantePthreads ();
bool compararResultados ();

#endif
