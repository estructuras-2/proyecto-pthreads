
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<math.h>
#include<stdbool.h>
#include<pi.h>
#include<matrizvector.h>


int main(){

  // CALCULO DE MULTIPLICACION DE MATRICES
  inicializacion(); // Inicializa las matrices y las variables auxiliares
  vectorResultanteNormal(); // Calcula la multiplicación mediante un solo hilo
  vectorResultantePthreads(); // Calcula la multiplicación por n hilos, donde \
                                 la  matriz es de tamaño nxn
  int estadoComparacion = compararResultados (); // Compara ambas matrices, \
                                                    regresa 0 si son iguales y \
                                                    1 si son diferentes

  // Variables que almacenan las aproximaciones de pi
  double piMono, piMulti, aux;

  piMono = calcPiMono(); // Se calcula en valor de pi utilizando un solo hilo

  piMulti = calcPiMulti(); // Se calcula el valor de pi utilizando 4 hilos

  printf("Pi calculado con un sólo hilo %.13f\n", piMono);
  printf("Pi calculado con un 4 hilos %.13f\n", piMulti);

  aux = piMono - piMulti; // Calcula la cota de error de las aproximaciones
  aux = fabs(aux); // Calcula el valor absoluto de la cota de error

  // Si la cota de error es menor a 1x10^-13 y si las matrices comparadas son \
     iguales
  if(aux < 0.0000000000001 && estadoComparacion == 0){
    return 0;
  }
  else{
    return -1;
  }

}
