#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<math.h>

// Se define una variable global que almacena el valor de pi calculado \
   mediante paralelismo, además se crea un mutex para bloquear esta variable
double piMulti;
pthread_mutex_t mutexPi;

// Esta función es el punto de entrada de los hilos
void *hilo(void *param){
  // Primero convierte los parámetros en variables utilizables
  int* parametros = (int*) param;
  int inicio = parametros[0];
  int final = parametros[1];

  double piParcial = 0; // Guarda el valor de la aproximación
  double aux = 0; // Auxiliar para guardar el signo de la iteración

  // Se aproxima pi mediante la serie de Leibniz, donde se utiliza la serie de \
     arctan con x = 1 para aproximar el valor de pi/4 = 1 - 1/3 + 1/5 - 1/7... \
     De modo que al despejar para pi se obtiene una aproximación de este valor
  for(int i = inicio; i < final; i++){
    aux = pow(-1, i); // Signo de la iteración
    pthread_mutex_lock (&mutexPi); // Se intenta bloquear el mutex
    piMulti = piMulti + (aux * 4) / (2 * i + 1); // Siguiente iteración de la \
                                                    serie
    pthread_mutex_unlock (&mutexPi); // Se desbloquea el mutex
  }

  return NULL;
}

// Esta función calcula en valor de pi meidante el uso de un sólo hilo
double calcPiMono(){

  double pi = 0; // Guarda el valor de la aproximación
  double aux = 0; // Auxiliar para guardar el signo de la iteración

  // Se aproxima pi mediante la serie de Leibniz, donde se utiliza la serie de \
     arctan con x = 1 para aproximar el valor de pi/4 = 1 - 1/3 + 1/5 - 1/7... \
     De modo que al despejar para pi se obtiene una aproximación de este valor
  for(int i = 0; i < 500000; i++){
    aux = pow(-1, i); // Signo de la iteración
    pi = pi + (aux * 4) / (2 * i + 1); // Siguiente iteración de la serie
  }

  return pi; // Regresa el valor de la aproximación

  printf("Valor de pi aproximado por un sólo hilo = %f\n", pi);

  return pi;
}

// Esta función calcula el valor de pi mediante el uso de n hilos
double calcPiMulti(){
  pthread_mutex_init(&mutexPi, NULL); // Se inicializa el mutex

  pthread_mutex_lock (&mutexPi); // Se intenta bloquear el mutex
  piMulti = 0; // Una vez que se bloquea, se inicializa el valor de pi a 0
  pthread_mutex_unlock (&mutexPi); // Se desbloquea el mutex

  // Se crean 4 hilos
  pthread_t hilo1;
  pthread_t hilo2;
  pthread_t hilo3;
  pthread_t hilo4;

  // Esta variable almacena el número de iteraciones que debe hacer cada hilo
  int incremento = 500000 / 4;

  // Se crea un array que almacena los límites de las iteraciones que debe
  // realizar cada hilo
  int paramsH1[2];
  int paramsH2[2];
  int paramsH3[2];
  int paramsH4[2];

  paramsH1[0] = 0;
  paramsH1[1] = incremento;
  paramsH2[0] = incremento;
  paramsH2[1] = incremento * 2;
  paramsH3[0] = incremento * 2;
  paramsH3[1] = incremento * 3;
  paramsH4[0] = incremento * 3;
  paramsH4[1] = incremento * 4;

  // Se ejecutan los 4 hilos repartiendo la cantidad de iteraciones
  // para aproximar pi entre todos de manera equitativa
  pthread_create(&hilo1, NULL, hilo, &paramsH1);
  pthread_create(&hilo2, NULL, hilo, &paramsH2);
  pthread_create(&hilo3, NULL, hilo, &paramsH3);
  pthread_create(&hilo4, NULL, hilo, &paramsH4);

  // Se espera a que todos los hilos hayan acabado
  pthread_join(hilo1, NULL);
  pthread_join(hilo2, NULL);
  pthread_join(hilo3, NULL);
  pthread_join(hilo4, NULL);

  // Elimina el mutex
  pthread_mutex_destroy(&mutexPi);

  return piMulti;
}
