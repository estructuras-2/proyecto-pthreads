#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<unistd.h>
#include<sys/stat.h>
#include<fcntl.h>

#define k 10
#define maxSize 8

pthread_mutex_t mutex1;
int n;
int vectorResPthreads[maxSize];
int vectorResNormal[maxSize];
int matrizA[maxSize][maxSize];
int vectorA[maxSize];

void inicializacion ()
{
    srand (time(NULL));
    n = rand()%6 + 3; //EL TAMAÑO ES RANDOM
    //SE LLENA LA MATRIZ
    //printf("Ingresando los datos de la matriz \n");
    for(int i=0;i<n;i++)
    {
      for(int j=0;j<n;j++)
      {
        //Se llena manualmente
        //printf("Ingresa el elemento de la posicion [%d][%d]", indiceF,indiceC);
        //scanf("%d",&matrizA[indiceF][indiceC]);


        //Se llena automaticamente con valores aleatorios entre 0 y k-1, con k definido al principio del programa
        matrizA[i][j]=rand()%k;
      }
    }


    //SE LLENA EL VECTOR
    //printf("Ingresando los datos del vector \n");
    for(int i=0;i<n;i++)
    {
      //Se llena manualmente
      //printf("Ingresa el elemento de la posicion [%d]",indiceC); //Estas dos lineas son para llenar manualmente
      //scanf("%d",&vectorA[indiceC]);


      //Se llena automaticamente con valores aleatorios entre 0 y k-1, con k definido al principio del programa
      vectorA[i] = rand()%k;
    }

    for(int i=0;i<n;i++)
    {
      vectorResPthreads[i] = 0;
    }


}

int vectorResultanteNormal()
{

  //SE DEFINE EL TAMANO DE LA MATRIZ Y VECTOR, SE IMPRIME EN PANTALLA LA INFO


  printf("Las matriz generada es de tamaño: [%dx%d] \n", n,n);
  printf("El vector generado es de tamaño: [%dx1] \n", n);
  printf("\n");

  for(int i=0;i<n;i++)
  {
    vectorResNormal[i] = 0;
  }
  int indiceF=0, indiceC=0; //indices para Fila y Columna


  //SE MUESTRAN LOS DATOS DE LA MATRIZ EN PANTALLA
  printf("Mostrando los datos de la matriz generada con numeros aleatorios entre 0 y %d \n", k-1);
  for(indiceF=0;indiceF<n;indiceF++)
  {
    for(indiceC=0;indiceC<n;indiceC++)
    {
      printf("[%d]", matrizA[indiceF][indiceC]);
    }
    printf("\n");
  }


  //SE MUESTRAN LOS DATOS DEL VECTOR EN PANTALLA
  printf("Mostrando los datos del vector generado con numeros aleatorios entre 0 y %d \n", k-1);
  for(indiceC=0;indiceC<n;indiceC++)
  {
    printf("[%d]",vectorA[indiceC]);
  }
  printf("\n");

  //SE MULTIPLICA LA MATRIZ POR EL VECTOR
  printf("\nSe multiplica la matriz por el vector... \n\n");
  for(indiceF=0;indiceF<n;indiceF++)
  {
    for(indiceC=0;indiceC<n;indiceC++)
    {
      vectorResNormal[indiceF]+=matrizA[indiceF][indiceC]*vectorA[indiceC];
    }
  }

  //SE MUESTRAN LOS DATOS DEL RESULTADO DE LA MULTIPLICACION EN PANTALLA
  printf("Mostrando la matriz resultante de la multiplicación: \n");
  for(indiceF=0;indiceF<n;indiceF++)
  {
    printf("[%d]", vectorResNormal[indiceF]);
    printf("\n");
  }

    return 0;
}


void *thread_routine(void *arg)
{
  //cual fila está calculando
  int i = (int)arg;

  for(int indiceColumna=0;indiceColumna<n;indiceColumna++)
  {
    pthread_mutex_lock (&mutex1);
    vectorResPthreads[i]+=matrizA[i][indiceColumna]*vectorA[indiceColumna];
    pthread_mutex_unlock (&mutex1);
  }

  return NULL;
}



int vectorResultantePthreads ()
{


  pthread_mutex_init(&mutex1, NULL);

    pthread_t vectorDeThreads[n];

    // SE CREAN LOS n THREADS
    for(int i=0;i<n;i++)
    {
      pthread_create(&vectorDeThreads[i], NULL, thread_routine, (void *)i);
    }


    //DEPUES DEL CALCULO SE VUELVEN A UNIR
    for(int i=0;i<n;i++)
    {
      pthread_join(vectorDeThreads[i], NULL);

    }



    printf("Mostrando la matriz resultante de la multiplicación para los Pthreads: \n");
    for(int i=0;i<n;i++)
    {
      printf("[%d]", vectorResPthreads[i]);
      printf("\n");
    }

   pthread_mutex_destroy(&mutex1);
   return 0;
}



int compararResultados (){

  int estado = 0; // Indica que los vectores son iguales

  for (int i = 0; i < n; i++) // Recorre los vectores
  {
    if ((vectorResNormal[i]!=vectorResPthreads[i])) // Compara las entradas de los vectores
    {
      estado =  1; // Si las entradas son iguales, cambia estado a diferentes
    }
  }

  return estado;

}
