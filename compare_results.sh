#!/bin/bash

# This scripts does a simple comparison of the exit status of a program

# Run program
./build/src/paralelismo

if (($? == -1));then
  exit -1
fi
