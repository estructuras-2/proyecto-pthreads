# Proyecto Pthreads

Este repositorio resuelve la tarea pthreads del curso IE0521 - Estructuras de computadoras digitales 2. Consiste en dos programas que resuelven distintas tareas asignadas:

1. Cálculo de la multiplicación de una matriz nxn por un vector nx1.
2. Aproximación de Pi mediante la serie de Leibniz.

Estos dos ejercicios se resuelven primero mediante un sólo hilo y luego utilizando paralelismo.

Este repositorio cuenta con el sistema de integración contínua de GitLab, y ejecuta un script que compila el código y compara el estado de salida del programa principal con el esperado. 

## Cómo compilar el programa

Este proyecto utiliza cmake para la compilación del código
Es necesario crear un directorio build y correr los comandos dentro
```
>> mkdir build
>> cd build
>> cmake ..
>> make
```

## Cómo correr el programa

El ejecutable se guarda dentro de build/src y puede ser ejecutado mediante el comando:
```
./src/paralelismo
```

### Dependencias

Es necesario asegurarse de que CMake se encuentra instalado:
```
sudo apt-get install libgtest-dev

sudo apt-get install cmake # install cmake
```